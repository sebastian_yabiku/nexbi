import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptRouterModule } from "nativescript-angular/router";

// libs
import { routeBaseTemPlatePublic } from '@nexbi/features';

// xplat
import { UIModule } from '@nexbi/nativescript/features/ui/ui.module';
import { TEMPUBLIC_COMPONENTS, TemPublicComponent } from '@nexbi/nativescript/features/tem-public/components';


// routes
const ROUTE_BASE_TEMPLATE_PUBLIC = routeBaseTemPlatePublic(
  {
    home: '../pag-home/pag-home.module#PagHomeModule'
  }, 
  {
    templatePublic: TemPublicComponent
  }
)

@NgModule({
  imports: [
    UIModule,
    NativeScriptRouterModule.forChild(ROUTE_BASE_TEMPLATE_PUBLIC)
  ],
  declarations: [...TEMPUBLIC_COMPONENTS],
  exports: [...TEMPUBLIC_COMPONENTS],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TemPublicModule {}
