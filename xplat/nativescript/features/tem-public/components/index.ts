import { TemPublicComponent } from '@nexbi/nativescript/features/tem-public/components/tem-public/tem-public.component';

export const TEMPUBLIC_COMPONENTS = [TemPublicComponent];

export * from '@nexbi/nativescript/features/tem-public/components/tem-public/tem-public.component';
