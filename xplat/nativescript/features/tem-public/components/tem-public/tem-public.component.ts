import { Component } from '@angular/core';

import { TemPublicBaseComponent } from '@nexbi/features';

@Component({
  moduleId: module.id,
  selector: 'ibk-tem-public',
  templateUrl: './tem-public.component.html'
})
export class TemPublicComponent extends TemPublicBaseComponent {
  constructor() {
    super();
  }
}
