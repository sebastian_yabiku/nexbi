import { PagHomeComponent } from '@nexbi/nativescript/features/pag-home/components/pag-home/pag-home.component';

export const PAGHOME_COMPONENTS = [PagHomeComponent];

export * from '@nexbi/nativescript/features/pag-home/components/pag-home/pag-home.component';
