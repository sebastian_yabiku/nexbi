import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptRouterModule } from "nativescript-angular/router";

// libs
import { routeBasePageHome } from '@nexbi/features';

import { UIModule } from '@nexbi/nativescript/features/ui/ui.module';
import { PAGHOME_COMPONENTS, PagHomeComponent } from '@nexbi/nativescript/features/pag-home/components';

const ROUTE_BASE_PAGE_HOME = routeBasePageHome(false,{
  index: PagHomeComponent
})

@NgModule({
  imports: [
    UIModule,
    NativeScriptRouterModule.forChild(ROUTE_BASE_PAGE_HOME)
  ],
  declarations: [...PAGHOME_COMPONENTS],
  exports: [...PAGHOME_COMPONENTS],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PagHomeModule {}
