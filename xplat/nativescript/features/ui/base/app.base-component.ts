import { Component } from '@angular/core';

// libs
import { BaseComponent } from '@nexbi/core';
import { AppService } from '@nexbi/nativescript/core';

export abstract class AppBaseComponent extends BaseComponent {
  constructor(protected appService: AppService) {
    super();
  }
}
