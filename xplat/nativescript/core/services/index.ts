import { AppService } from '@nexbi/nativescript/core/services/app.service';
import { TNSWindowPlatformService } from '@nexbi/nativescript/core/services/tns-window.service';

export const PROVIDERS: any[] = [AppService, TNSWindowPlatformService];

export * from '@nexbi/nativescript/core/services/app.service';
