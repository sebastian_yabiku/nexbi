import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';

// libs
import { routeBasePageHome } from '@nexbi/features';

import { UIModule } from '@nexbi/web/features/ui/ui.module';
import { PAGHOME_COMPONENTS, PagHomeComponent} from '@nexbi/web/features/pag-home/components';

const ROUTE_BASE_PAGE_HOME = routeBasePageHome(false,{
  index: PagHomeComponent
})

@NgModule({
  imports: [
    UIModule, 
    RouterModule.forChild(ROUTE_BASE_PAGE_HOME)
  ],
  declarations: [...PAGHOME_COMPONENTS],
  exports: [...PAGHOME_COMPONENTS]
})
export class PagHomeModule {}
