import { Component } from '@angular/core';

import { PagHomeBaseComponent } from '@nexbi/features';

@Component({
  selector: 'ibk-pag-home',
  templateUrl: 'pag-home.component.html'
})
export class PagHomeComponent extends PagHomeBaseComponent {
  constructor() {
    super();
  }
}
