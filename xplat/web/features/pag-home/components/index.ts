import { PagHomeComponent } from '@nexbi/web/features/pag-home/components/pag-home/pag-home.component';

export const PAGHOME_COMPONENTS = [PagHomeComponent];

export * from '@nexbi/web/features/pag-home/components/pag-home/pag-home.component';
