import { TemPublicComponent } from '@nexbi/web/features/tem-public/components/tem-public/tem-public.component';

export const TEMPUBLIC_COMPONENTS = [TemPublicComponent];

export * from '@nexbi/web/features/tem-public/components/tem-public/tem-public.component';
