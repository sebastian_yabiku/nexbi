import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// libs
import { routeBaseTemPlatePublic } from '@nexbi/features';

// xplat
import { UIModule } from '@nexbi/web/features/ui/ui.module';
import {
  TEMPUBLIC_COMPONENTS,
  TemPublicComponent
} from '@nexbi/web/features/tem-public/components';

// routes
const ROUTE_BASE_TEMPLATE_PUBLIC = routeBaseTemPlatePublic(
  {
    home: '../pag-home/pag-home.module#PagHomeModule'
  }, 
  {
    templatePublic: TemPublicComponent
  }
)

@NgModule({
  imports: [
    UIModule, 
    RouterModule.forChild(ROUTE_BASE_TEMPLATE_PUBLIC)
  ],
  declarations: [...TEMPUBLIC_COMPONENTS],
  exports: [...TEMPUBLIC_COMPONENTS]
})
export class TemPublicModule { }
