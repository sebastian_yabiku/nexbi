import { LogService } from '@nexbi/core/services/log.service';
import { WindowService } from '@nexbi/core/services/window.service';

export const CORE_PROVIDERS: any[] = [LogService, WindowService];

export * from '@nexbi/core/services/log.service';
export * from '@nexbi/core/services/window.service';
export * from '@nexbi/core/services/tokens';
