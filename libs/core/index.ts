export * from '@nexbi/core/base';
export * from '@nexbi/core/environments/environment';
export * from '@nexbi/core/services';
export { CoreModule } from '@nexbi/core/core.module';
export * from '@nexbi/core/routes.path'