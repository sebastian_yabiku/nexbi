import { Routes } from '@angular/router';
import { ROUTES_TEMPLATES } from '@nexbi/core';

interface IModules {
    public: string;
}

export function routeBaseTemPlates(modules: IModules): Routes {
    return [
        {
            path: ROUTES_TEMPLATES.public,
            loadChildren: modules.public
        }
    ];
}