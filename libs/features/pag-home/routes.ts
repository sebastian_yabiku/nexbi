import { Routes } from '@angular/router';
import { ROUTES_HOME } from '@nexbi/core';

interface IModules {
}
interface IComponents {
    index: any;
}
export function routeBasePageHome(modules: IModules = {}, components: IComponents): Routes {
    return [
        {
            path: ROUTES_HOME.index,
            component: components.index
        }
    ];
}