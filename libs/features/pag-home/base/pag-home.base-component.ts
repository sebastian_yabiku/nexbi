import { BaseComponent } from '@nexbi/core';

export abstract class PagHomeBaseComponent extends BaseComponent {
  public text: string = 'PagHome';

  constructor() {
    super();
  }
}
