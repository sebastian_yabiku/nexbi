// mod angular
import { Routes } from '@angular/router';

// libs
import { ROUTES_TEMPLATE_PUBLIC } from '@nexbi/core';

interface IModules {
    home: string;
}
interface IComponents {
    templatePublic: any;
}
export function routeBaseTemPlatePublic(modules: IModules, components: IComponents): Routes {
    return [
        {
            path: ROUTES_TEMPLATE_PUBLIC.home,
            component: components.templatePublic,
            children: [
                {
                    path: ROUTES_TEMPLATE_PUBLIC.home,
                    loadChildren: modules.home
                }                
            ]            
        }
    ];
}