import { BaseComponent } from '@nexbi/core';

export abstract class TemPublicBaseComponent extends BaseComponent {
  public text: string = 'TemPublic';

  constructor() {
    super();
  }
}
