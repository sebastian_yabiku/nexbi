import { Component } from '@angular/core';

// xplat
import { AppBaseComponent } from '@nexbi/web';

@Component({
  selector: 'ibk-root',
  templateUrl: './app.component.html'
})
export class AppComponent extends AppBaseComponent {
  constructor() {
    super();
  }
}
