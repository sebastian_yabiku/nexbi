import { NgModule } from '@angular/core';

// xplat
import { IbkCoreModule } from '@nexbi/web';

@NgModule({
  imports: [IbkCoreModule]
})
export class CoreModule {}
