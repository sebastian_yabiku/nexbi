import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

// libs
import { routeBaseTemPlates } from '@nexbi/features';

// app
import { CoreModule } from './core/core.module';
import { SharedModule } from './features/shared/shared.module';
import { AppComponent } from './app.component';

// route
const ROUTE_BASE_TEMPLATES = routeBaseTemPlates({
  public: './features/tem-public/tem-public.loader.module#TemPublicLoader'
})

@NgModule({
  imports: [
    CoreModule, 
    SharedModule,
    RouterModule.forRoot(ROUTE_BASE_TEMPLATES)
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
