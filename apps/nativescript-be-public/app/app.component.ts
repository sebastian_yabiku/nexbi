import { Component } from '@angular/core';

// libs
import { AppBaseComponent, AppService } from '@nexbi/nativescript';

@Component({
  selector: 'ibk-root',
  template: `<page-router-outlet></page-router-outlet>`
})
export class AppComponent extends AppBaseComponent {

  constructor(
    appService: AppService
  ) {
    super(appService);
  }
}
