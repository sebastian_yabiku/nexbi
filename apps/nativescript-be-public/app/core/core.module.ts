import { NgModule } from '@angular/core';

// libs
import { IbkCoreModule } from '@nexbi/nativescript';

@NgModule({
  imports: [IbkCoreModule]
})
export class CoreModule {}
