import { NgModule } from '@angular/core';

// xplat
import { TemPublicModule } from '@nexbi/nativescript';

const MODULES = [TemPublicModule];

@NgModule({
  imports: [...MODULES],
  exports: [...MODULES]
})
export class TemPublicLoader {}
